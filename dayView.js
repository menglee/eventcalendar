var restoredSession;
function showDayView() {

	var keyString = sessionStorage.getItem("request");
	var toks = keyString.split(" ");
	var m = parseInt(toks[0]);
	var d = parseInt(toks[1]);
	var y = parseInt(toks[2]);

	restoredSession = JSON.parse(localStorage.getItem("mapSession"));

	var topDiv = document.getElementById("dateDiv");
	topDiv.innerHTML = genDate(m, d, y);

	var viewDiv = document.getElementById("dayDiv");
	viewDiv.innerHTML = genEventTable(m, d);
}

function genDate(keyMonth, keyDay, keyYear) {
	var html = "";

	html += "<h1>" + getCurrMonthString(keyMonth) + ' ' + keyDay + ', ' + keyYear + "</h1>";
	html += "<button class=\'daybtns\' onclick=\'goToMonthView()\'>Back</button>";
	html += "&nbsp&nbsp";
	html += "<button class=\'daybtns\' onclick=\'addEvent()\'>New Event</button>";
	html += "<hr id=\'hr100\'><br>";
	return html;
}

function goToMonthView() {

	var keyString = sessionStorage.getItem("request");
	var toks = keyString.split(" ");
	var m = parseInt(toks[0]);
	var d = parseInt(toks[1]);
	var y = parseInt(toks[2]);

	sessionStorage.setItem("tempDay", m.toString() + " " + d.toString() + " " + y.toString());

	localStorage.setItem("nextSession", JSON.stringify(restoredSession));

	location.assign("grid.html");
}

function addEvent() {

	var keyString = sessionStorage.getItem("request");
	var toks = keyString.split(" ");
	var m = parseInt(toks[0]);
	var d = parseInt(toks[1]);
	var y = parseInt(toks[2]);

	var topDiv = document.getElementById("dateDiv");
	topDiv.innerHTML = genDate(m, d, y);

	var hide = document.getElementById("userInputDiv");
	hide.style.display ='inline';

	var inputDiv = document.getElementById("userInputDiv");
	inputDiv.innerHTML = genFields();

	var viewDiv = document.getElementById("dayDiv");
	viewDiv.innerHTML = genEventTable(m, d);
}

function genFields() {
	var html = "";

	html += "start time: &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp";
	html += "<input type=\'text\' id=\'startTimeText\'>";
	html += "<br><br>";
	html += "duration: &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp";
	html += "<input type=\'text\' id=\'durationText\'>";
	html += "<br><br>";
	html += "name of event: &nbsp&nbsp&nbsp";
	html += "<input type=\'text\' id=\'nameText\'>";
	html += "<br><br>";
	html += "place: &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp";
	html += "<input type=\'text\' id=\'placeText\'>";
	html += "<br><br>";
	html += "notes: &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp";
	html += "<input type=\'text\' id=\'notesText\'>";
	html += "<br><br>";
	html += "<button class=\'daybtns\' onclick=\'makeEvent()\'>Add Event</button>";
	html += "<br><br>";

	return html;
}

function makeEvent() {
	var keyString = sessionStorage.getItem("request");
	var toks = keyString.split(" ");
	var m = parseInt(toks[0]);
	var d = parseInt(toks[1]);
	var y = parseInt(toks[2]);
	
	var o = new evnt();
	o.month = m;
	o.year = y;
	o.day = d;
	
	o.name = document.getElementById("nameText").value;
	o.startTime = document.getElementById("startTimeText").value;
	o.duration = document.getElementById("durationText").value;
	o.place = document.getElementById("placeText").value;
	o.notes = document.getElementById("notesText").value;

	restoredSession.push(o);

	var hide = document.getElementById("userInputDiv");
	hide.style.display ='none';

	var topDiv = document.getElementById("dateDiv");
	topDiv.innerHTML = genDate(m, d, y);
	
	var viewDiv = document.getElementById("dayDiv");
	viewDiv.innerHTML = genEventTable(m, d);
}

function genEventTable(keyMonth, keyDay) {
	var html = "";

	var i = 0;
	var j = 0;
	var k = 0;
	var maxRow = 0;

	var temp = new Array();
	for (i = 0; i < restoredSession.length; i++) {
		if (restoredSession[i].month == keyMonth && restoredSession[i].day == keyDay) {
			maxRow += 1;
			temp.push(restoredSession[i].startTime);
			temp.push(restoredSession[i].duration);
			temp.push(restoredSession[i].name);
			temp.push(restoredSession[i].place);
			temp.push(restoredSession[i].notes);
		}
	}
	if (maxRow) {
		maxRow += 1;
	}
	var maxCol = 5;

	var header = ["START TIME", "DURATION", "NAME", "PLACE", "NOTES"];

	html += "<table id=\'t100\'>";
	
	for (i = 0; i < maxRow; i++) {
		html += "<tr>";
		for (j = 0; j < maxCol; j++) {
			if (i == 0) {
				html += "<th>" + header[j] + "</th>";
			}
			else {
				html += "<td>" + temp[k] + "</td>";
				k += 1;
			}
		}
	}
	html += "</table>";
	return html;
}