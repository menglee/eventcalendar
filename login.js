function showLogin() {

	var logDiv = document.getElementById("loginDiv");
	logDiv.innerHTML = genTextFields();
}

function genTextFields() {
	var html = "";

	html += "username: ";
	html += "<input type=\'text\' id=\'usernameText\'>";
	html += "<br><br>";
	html += "password: ";
	html += "<input type=\'password\' id=\'passwordText\'>";
	html += "<br><br>";
	html += "<button onclick=\'validate()\'>Log In</button>";
	return html;
}

function validate() {
	var userName = document.getElementById("usernameText").value;
	var passWord = document.getElementById("passwordText").value;

	var keyPair = "userName=" + userName + "&password=" + passWord;

	var localRequest = getXMLHttpRequestObject();

	localRequest.open("POST", "http://universe.tc.uvu.edu/cs2550/assignments/PasswordCheck/check.php", false);
	localRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	localRequest.send(keyPair);

	if (localRequest.status == 200) {}
	
	var msgDiv = document.getElementById("loginMsgDiv");

	var responseJson = JSON.parse(localRequest.responseText);

	if (responseJson["result"] == "valid") {

		var info = responseJson["userName"] + " " + responseJson["timestamp"];

		localStorage.setItem("cs2550timestamp", info);

		location.assign("grid.html");
	}
	else {
		msgDiv.innerHTML = genLogFailMsg();
	}
}

function genLogFailMsg() {
	var html = "";
	html += "Incorrect user name or password!";
	return html;
}