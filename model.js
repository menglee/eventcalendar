function initialize() {
	var d = new Date();
	state.month = d.getMonth();
	state.year = d.getFullYear();

	getEvents();
}

//from professor Durney's example of using XML data in an HTML document.
//http://universe.tc.uvu.edu/cs2550/assignments/XML/addresses.js
function getEvents() {
	var request = new XMLHttpRequest();
	request.open("GET", "data.xml", false);
	request.send(null);

	var xmldoc = request.responseXML;

	var xmlrows = xmldoc.getElementsByTagName("event");

	for (var r = 0; r < xmlrows.length; r++) {
		var xmlrow = xmlrows[r];

		var o = new evnt();

		var xmonth = xmlrow.getElementsByTagName("month")[0];
		o.month = parseInt(xmonth.firstChild.data);

		var xyear = xmlrow.getElementsByTagName("year")[0];
		o.year = parseInt(xyear.firstChild.data);

		var xday = xmlrow.getElementsByTagName("day")[0];
		o.day = parseInt(xday.firstChild.data);

		var xtime = xmlrow.getElementsByTagName("time")[0];
		o.startTime = xtime.firstChild.data;

		var xduration = xmlrow.getElementsByTagName("duration")[0];
		o.duration = xduration.firstChild.data;

		var xname = xmlrow.getElementsByTagName("name")[0];
		o.name = xname.firstChild.data;

		var xplace = xmlrow.getElementsByTagName("place")[0];
		o.place = xplace.firstChild.data;

		var xnotes = xmlrow.getElementsByTagName("notes")[0];
		o.notes = xnotes.firstChild.data;

		map.push(o);
	}

	//alert(map.length.toString());
}

function getCurrMonthString(number) {
	var monthof = ["JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE",
	"JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER"];

	return monthof[number];		// returns month as string
}

// http://en.wikipedia.org/wiki/Determination_of_the_day_of_the_week
function getFirstDay() {
	var yy = parseInt(state.year.toString().substr(2,2));
	var monthCodes = [0,3,3,6,1,4,6,2,5,0,3,5];
	var mm = monthCodes[state.month];
	var d = (1 + mm + yy + Math.floor(yy/4) + 6) % 7;
	return d;
}

function maxDays(cmonth) {
	var arr = [31,28,31,30,31,30,31,31,30,31,30,31];
	return arr[cmonth];
}

//---------------------My Model----------------------
var map = new Array();

var state = {
	month: null,
	day: null,
	year: null,
};

var clickedDay = "";

function evnt() {
	var month = null,
	year = null,
	day = null,
	name = null,
	startTime = null,
	duration = null,
	place = null,
	notes = null;
}

// var object = {};
// var list = [];
// var eventObj = ["6pm", "4hrs", "Valentines Day", "Fancy Restaurant", "Dress Formal"];
// list.push(eventObj);

// object = {"1 14": list};

// //alert(object["1 14"][0]);

// var eventObj = ["7pm", "5hrs", "Valentines Day", "Anything", "Anything"];

// object["1 14"].push(eventObj);

// //alert(object["1 14"][1]);

//------------ For assignment 3 ------------------
// making test objects for displaying my model

// var o = new evnt();
// o.month = 1;
// o.year = 2015;
// o.day = 14;
// o.name = "Valentines Day";
// o.startTime = "6pm";
// o.duration = "4hrs";
// o.place = "Fancy Restaurant";
// o.notes = "Dress Formal";

// map.push(o);

// o = new evnt();
// o.month = 1;
// o.year = 2015;
// o.day = 16;
// o.name = "Assignment 3 Due";
// o.startTime = "11.59pm";
// o.duration = "n/a";
// o.place = "Canvas";
// o.notes = "Turn in zip file on canvas";

// map.push(o);

// o = new evnt();
// o.month = 1;
// o.year = 2015;
// o.day = 14;
// o.name = "Testing Multiple Events";
// o.startTime = "11am";
// o.duration = "All day";
// o.place = "Home";
// o.notes = "Testing";

// map.push(o);