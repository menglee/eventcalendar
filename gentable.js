function showTable() {

	// use other functions from model.js to start initializing starting parameters.
	initialize();

	var reload = sessionStorage.getItem("tempDay");
	if (reload != null) {
		var line = reload.split(" ");
		state.month = parseInt(line[0]);
		state.day = parseInt(line[1]);
		state.year = parseInt(line[2]);

		map = JSON.parse(localStorage.getItem("nextSession"));
		
		sessionStorage.removeItem("tempDay");
	}

	var monthDiv = document.getElementById("monthDiv");
	monthDiv.innerHTML = genMonth(state.month, state.year);

	var tableDiv = document.getElementById("gridDiv");
	tableDiv.innerHTML = genTable(6, 7, getFirstDay());

	var userDiv = document.getElementById("userInfoDiv");
	userDiv.innerHTML = genInfoMsg();

	//startMove();

	addClickHandlers();
}

function genMonth(currMonth, currYear) {
	var html = "";

	html += "<h1>" + getCurrMonthString(currMonth) + ' ' + currYear + "</h1>";
	html += "<button class=\'btn\' onclick=\'previousMonth()\'>Previous</button>";
	html += "&nbsp&nbsp";
	html += "<button class=\'btn\' onclick=\'nextMonth()\'>Next</button>";
	html += "<hr id=\'hr01\'>";
	return html;
}

function nextMonth() {
	state.month++;
	if (state.month == 12) {
		state.year++;
		state.month = 0;
	}

	var monthDiv = document.getElementById("monthDiv");
	monthDiv.innerHTML = genMonth(state.month, state.year);

	var tableDiv = document.getElementById("gridDiv");
	tableDiv.innerHTML = genTable(6, 7, getFirstDay());

	addClickHandlers();
}

function previousMonth() {
	state.month--;
	if (state.month == -1) {
		state.year--;
		state.month = 11;
	}

	var monthDiv = document.getElementById("monthDiv");
	monthDiv.innerHTML = genMonth(state.month, state.year);

	var tableDiv = document.getElementById("gridDiv");
	tableDiv.innerHTML = genTable(6, 7, getFirstDay());

	addClickHandlers();
}

function genTable(rows, cols, dayCode) {
	var html = "";
	var i = 0;
	var j = 0;
	var k = 0;
	var maxRow = rows;
	var maxCol = cols;
	var box = 1;
	var day = 1;

	var counter = 0;

	var eventCounter = 0;

	var hClass = "ODD";

	var dayOf = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

	var offset = dayCode;

	html += "<table id=\'t01\'>"
	for (i = 0; i < maxRow; i++) {
		html += "<tr>"
		for (j = 0; j < maxCol; j++) {
			if (i == 1) {
				offset--;
			}
			if (i == 0) {
				html += "<th>" + dayOf[j] + "</th>"
			}
			else {
				if (box % 2 == 0) {
					hClass = "EVEN";
				}
				else {
					hClass = "ODD";
				}
				if (offset < 0) {
					if (day > maxDays(state.month)) {
						html += "<td class=\'" + hClass + "\'></td>";
					}
					else{
						for (k = 0; k < map.length; k++) {
							if (map[k].month == state.month && map[k].year == state.year && map[k].day == day) {
								eventCounter += 1;
							}
						}
						if (eventCounter) {
							html += "<td class=\'" + hClass + "\'>" + day + " <br><br>" +"Events: " + eventCounter + "</td>";
							eventCounter = 0;
						}
						else {
							html += "<td class=\'" + hClass + "\'>" + day + " " + "</td>";
						}
					}
					day += 1;
				}
				else{
					html += "<td class=\'" + hClass + "\'></td>";
				}
				box += 1;
			}
		}
	}
	html += "</table>";
	return html;
}

function addClickHandlers() {
	var tableDiv = document.getElementById("gridDiv");

	var cells = tableDiv.getElementsByTagName("td");

	for (var i = 0; i < cells.length; i++) {
		cells[i].onclick = function() {
			var col = this.cellIndex;
			var row = this.parentNode.rowIndex;
			this.style.backgroundColor = "Orange";

			//var p4Div = document.getElementById("P4demo");
			//p4Div.innerHTML = genMessage(row, col);

			var line = this.innerHTML;
			clickedDay = line.split(" ");

			if (clickedDay.length == 1) {
			
			}
			else {
				sessionStorage.setItem("request", state.month.toString() + " " + clickedDay[0] + " " + state.year.toString());

				localStorage.setItem("mapSession", JSON.stringify(map));

				location.assign("sampleDayView.html");
			}
		}
	}
}

function genInfoMsg() {
	var html = "";
	html += "Logged in as: ";
	html += localStorage.getItem("cs2550timestamp");
	html += "&nbsp&nbsp&nbsp&nbsp";
	html += "<button onclick=\'clearStorage()\'>clear</button>";
	return html;
}

function clearStorage() {
	localStorage.removeItem("cs2550timestamp");
	alert("removed cs2550timestamp from local storage!");
}

// function genMessage(r, c) {
// 	var r = r -1;
// 	html = "";
// 	html += "<h1>row: " + r + ", col: " + c + "</h1>";
// 	return html;
// }

/*var foo = null;
var botLeft;
function startMove() {
	foo = document.getElementById('animatedemo');
	foo.style.left = '0px';
	foo.style.bottom = '0px';
	botLeft = 0;
	doMove();
}

function doMove() {
	botLeft += 20;
	foo.style.bottom = parseInt(foo.style.bottom)+1+'px';
	foo.style.left = parseInt(foo.style.left)+1+'px';

	if (botLeft < 1500)
	{
		setTimeout(doMove, 50);
	}
	else {
		botLeft = 0;
		moveAgain();
	}
}

function moveAgain() {
	botLeft += 20;
	foo.style.bottom = parseInt(foo.style.bottom)-1+'px';
	foo.style.left = parseInt(foo.style.left)+1+'px';

	if (botLeft < 1500) {
		setTimeout(moveAgain, 50);
	}
}*/